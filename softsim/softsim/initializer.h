//  initializer.h
//  Simulation Methods course, 2018
//  First Assignment: Molecular Dynamics (Brownian Dynamics) Simulation

#ifndef initializer_h
#define initializer_h

#include <stdio.h>
#include <stdlib.h>

void init_simulation(void);
void init_simulation_box(void);

void init_particles(void);
void init_particles_randomly(void);

void init_pinningsites_randomly(int number_of_pinningsites, double pinningsite_force);

char * remove_dots_from_string(const char * string_to_change);

void init_files(const char *number_of_pinningsites, const char *pinningsite_force);

double distance_folded_PBC(double x0,double y0,double x1,double y1);

#endif /* initializer_h */
